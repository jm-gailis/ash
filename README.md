# Ash

_Are Students Here?_

Ash is a program to register the presence of students, especially in school libraries.

This program is currently only available in **french**, and it's still a **Work In Progress**.


Ash is licensed under the GNU General Public Licence 3.

You should have received a copy of the GPLv3 licence along with this program.

If not, it is available [here](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
