#!/usr/bin/python3
from http.server import HTTPServer, CGIHTTPRequestHandler
port = 80

httpd = HTTPServer(('', port), CGIHTTPRequestHandler)

print('Serveur HTTP lancé sur : ' + str(httpd.server_port))
httpd.serve_forever()
